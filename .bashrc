alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd......='cd ../../../../..'
alias cd.......='cd ../../../../../..'
alias ls='ls --color=always'
alias la='ls -A'
alias ll='ls -alF'
alias cp='cp -i'
alias rm='rm -i'
alias mv='mv -i'
alias gs='git status'
alias ga='git add'
alias gd='git diff'
alias grep='grep --color=auto'
alias :q=exit
alias :wq=exit
alias please='sudo $(fc -ln -1)' # equivalent to sudo !!

fcd() {
	cd $(find . -type d | fzf --height=70% --layout=reverse --border \
		--tiebreak=chunk,length,end --info=inline --margin=1 --query "$1")
}


export HISTCONTROL=ignoreboth
export EDITOR=vim
export VISUAL=vim
export HOST=localhost
export PATH=$PATH:/var/lib/flatpak/exports/bin


gb() {
        echo -n ' - (' && git branch 2>/dev/null | grep '^*' | colrm 1 2 | tr -d '\n' && echo  -n ')'
}
git_branch() {
        gb | sed 's/\ -\ ()//'
}
PS1="\u:\w\$(git_branch) \$ "
