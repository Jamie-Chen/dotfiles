vim9script
set nocompatible
# =====PLUGINS=================================================================
# Autodownloads vimplug and plugins if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -flo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

autocmd VimEnter * if (len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) != 0)
	\| PlugInstall --sync | source $MYVIMRC
	\| endif

plug#begin('~/.vim/plugged')

# Functionality
# tpope
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-characterize'

Plug 'godlygeek/tabular'
Plug 'mbbill/undotree'
Plug 'terryma/vim-smooth-scroll'
Plug 'voldikss/vim-floaterm'
Plug 'andymass/vim-matchup'
Plug 'mhinz/vim-startify'
# Plug 'neoclide/coc.nvim', {'branch': 'release'}

# Additional Vim objects
Plug 'kana/vim-textobj-user'
Plug 'michaeljsmith/vim-indent-object'

# Look
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/vim-peekaboo'
Plug 'ap/vim-buftabline'

# Colour Schemes
Plug 'altercation/vim-colors-solarized'
# Plug 'gruvbox-community/gruvbox'

# Syntax Concealing
Plug 'KeitaNakamura/tex-conceal.vim'
Plug 'tim-clifford/vim-venus'
Plug 'plasticboy/vim-markdown'
plug#end()


# =====VARIABLE OPTIONS========================================================
filetype indent off
g:tex_conceal_frac = 1
g:tex_conceal = "abdgms"

# var g:coc_filetype_map = {'tex': 'latex'}

g:markdown_folding = 0
g:vim_markdown_folding_disabled = 1

g:vim_markdown_new_list_item_indent = 0
g:vim_markdown_no_default_key_mappings = 1
g:vim_markdown_folding_style_pythonic = 1

g:netrw_banner = 0
g:netrw_liststyle = 3
g:netrw_list_hide = netrw_gitignore#Hide()
g:netrw_list_hide ..= ',' .. '\(^\|\s\s\)\zs\.\S\+'
g:netrw_browsex_viewer =  "firefox"

g:buftabline_numbers = 1
g:buftabline_indicators = 1

g:python_recommended_style = 0


# =====COLOUR SCHEME===========================================================
syntax on
set t_Co=16
# Solarized
	# colorscheme solarized
	# # Dark
	# 	# set background=dark
	# 	# highlight CursorLineNr ctermfg=Cyan
	# 	# highlight SpecialKey ctermfg=Green ctermbg=DarkGrey
	# 	# highlight Visual ctermfg=Grey ctermbg=Green
	# 	# highlight clear Search
	# 	# highlight Search ctermfg=Grey ctermbg=NONE cterm=Bold
	# 	# highlight IncSearch ctermfg=Grey ctermbg=Red  cterm=Bold
	# 	# highlight MatchParen ctermfg=Grey
	# # Light
	# 	# set background=light
	# 	# highlight CursorLineNr ctermfg=Green
	# 	# highlight SpecialKey ctermfg=Cyan ctermbg=White
	# 	# highlight clear Search
	# 	# highlight Search ctermfg=DarkGrey ctermbg=NONE cterm=Bold
	# 	# highlight IncSearch ctermfg=DarkGrey ctermbg=Red  cterm=Bold
	# 	# highlight MatchParen ctermfg=DarkGrey
# Gruvbox
	# var g:gruvbox_italic = 1
	# var g:gruvbox_termcolors = 16
	# colorscheme gruvbox
	# highlight CursorLineNr ctermfg=Yellow ctermbg=Black
	# highlight SpecialKey ctermfg=DarkGrey
	# highlight clear Search
	# highlight Search ctermfg=Grey ctermbg=NONE cterm=Bold
	# highlight IncSearch ctermfg=Grey ctermbg=Red  cterm=Bold
	# highlight ColorColumn ctermbg=Black
	# highlight LineNr ctermbg=Black
	# highlight MatchParen ctermfg=Grey

# Only for translucent background terminal
	# highlight Normal ctermbg=NONE
	# highlight Conceal ctermbg=NONE
	# highlight SignColumn ctermbg=NONE
	# highlight CursorLineNr ctermbg=NONE
	# highlight SpecialKey ctermbg=NONE

highlight CursorLineNr cterm=Bold
highlight clear SpellBad
highlight SpecialKey cterm=NONE
highlight Visual cterm=Bold
highlight MatchParen cterm=NONE ctermbg=NONE


# =====Settings Changes========================================================
set history=10000
set noswapfile
set autoread
set hidden
set title
set ttyfast
set lazyredraw
set nofixendofline
set encoding=utf-8
# Allow use of mouse - useful for phone
# set ttymouse=sgr

set linebreak
set nowrap
set autoindent
set tabstop=6
set shiftwidth=6
set textwidth=79
set colorcolumn=80
set noshowmatch

set ttimeout
set ttimeoutlen=50
set notimeout
set showcmd
set number
set relativenumber
# For systems without cursorlineopt, comment this out and uncomment the highlight
set cursorlineopt=number
# highlight cursorline term=NONE cterm=NONE
set cursorline
set signcolumn=yes

set list
set listchars=tab:\┆\ ,trail:␣,nbsp:·

set scrolloff=6
set scrolljump=-25
set sidescroll=6
set sidescrolloff=6

set backspace=indent,eol,start

set incsearch
set hlsearch

set nojoinspaces
set spell
set spelllang=en_gb

# Persistent undo
if has('persistent_undo')
	var target_path = expand('~/.vim/undodir')
	if !isdirectory(target_path)
		system('mkdir -p '..target_path)
	endif
	&undodir = target_path
	set undolevels=1000
	set undofile
endif

# File finding
set path+=**
set wildmode=longest:full,full
set wildignorecase
set wildignore=*.git/*,*.tags,tags,*.o,*.class,*.ccls-cache
set wildmenu

# Autocomplete
set omnifunc=syntaxcomplete#Complete

# Splits and buffers
set splitright
set splitbelow

# =====FOLDS===================================================================
# Callback: Fold level <- next line indent
def FoldMethod(linenum: number): number
	var ind: number = max([indent(linenum + 1), indent(linenum)])
	var width: number = &shiftwidth
	return ind / width
enddef

set foldmethod=expr
set foldexpr=FoldMethod(v:lnum)
# starts the file with all folds open
autocmd BufWinenter * normal zR

# TODO write custom fold function to work for markdown
def NeatFoldText(): string
	var line = ' ' .. substitute(getline(v:foldstart), '^\s*"\?\s*\|\s*"\?\s*{{' .. '{\d*\s*', '', 'g') .. ' '
	var lines_count = v:foldend - v:foldstart + 1
	var lines_count_text = '| ' .. printf("%10s", lines_count .. ' lines') .. ' |'
	var foldchar = matchstr(&fillchars, 'fold:\zs.')
	var foldtextstart = strpart(line, 1, (winwidth(0) * 2) / 3)
	var foldtextend = lines_count_text .. repeat(foldchar, 8)
	var foldtextlength = strlen(substitute(foldtextstart .. foldtextend, '.', 'x', 'g')) + &foldcolumn
	return foldtextstart .. repeat(foldchar, winwidth(0) - foldtextlength) .. foldtextend
enddef

set foldtext=NeatFoldText()

# =====MAPPINGS================================================================
# Double Backspace removes search highlighting
nnoremap <silent> <BS><BS> :nohlsearch<CR>

# Map Ctrl-Backspace to delete the previous word in insert mode.
noremap! <C-BS> <C-w>
noremap! <C-h>  <C-w>

# Easier split navigation
nnoremap <silent> <C-h> <C-w>h
nnoremap <silent> <C-j> <C-w>j
nnoremap <silent> <C-k> <C-w>k
nnoremap <silent> <C-l> <C-w>l

# Buffer navigation
nnoremap <silent> <C-n> :bnext<CR>
nnoremap <silent> <C-p> :bprevious<CR>

# Easy compile and remove temp files
:command Tex !pdflatex %
:command Bib !bibtex %:r

# Toggle undotree - works as Control Space too
nnoremap <C-@> :UndotreeToggle<CR>

# Floaterm
:command F FloatermNew
:command Ft FloatermToggle
:command Fn FloatermNext
:command Fp FloatermPrev

# Makes Y normal
nnoremap Y y$

# TODO learn to use ex mode, for now I'm scared
nnoremap Q <Nop>

# Maintains cursor position with yanking visual
vnoremap y ygv<Esc>
vnoremap Y ygv<Esc>

# Keeps block selected through indents
vnoremap < <gv
vnoremap > >gv

# Visual drag
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

# Smooth Scroll
noremap <silent> <C-u> :call smooth_scroll#up   ( &scroll   , 13 , 2 ) <CR>
noremap <silent> <C-d> :call smooth_scroll#down ( &scroll   , 13 , 2 ) <CR>
noremap <silent> <C-b> :call smooth_scroll#up   ( &scroll*2 , 18 , 4 ) <CR>
noremap <silent> <C-f> :call smooth_scroll#down ( &scroll*2 , 18 , 4 ) <CR>

# =====STATUS BAR AND TAB LINE=================================================
def Gitbranch(): string
    b:git_branch = trim(system("git -C " .. expand("%:h") .. " branch --show-current 2>/dev/null"))
    if b:git_branch ==# ""
	    return ""
    else
	    return "-\ (" .. b:git_branch .. ")"
    endif
enddef

augroup Gitget
    autocmd!
    autocmd BufEnter * b:fancy_branch = Gitbranch()
augroup END


set laststatus=2
set statusline=\ %f%m%r%h%w\ %{b:fancy_branch}%=%y\ %{&fenc==\"\"?&enc:&fenc}[%{&ff}]\ [%02p%%][%l/%L:%c]\ 
set showtabline=2
# TODO look into making my own personal tabline


# =========THIS SHOULD REALLY BE IN .VIM/AFTER,================================
# ======BUT FOR NOW ALL MY CONFIGS ARE JUST IN .VIMRC==========================
autocmd BufEnter * set formatoptions=jnq

augroup ftmd
	autocmd!
	autocmd FileType venus setlocal conceallevel=2
	autocmd FileType venus highlight clear Error
augroup END
