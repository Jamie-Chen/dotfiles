-- IMPORTS
-- TODO Clean up the inports to be way more orderly
import XMonad
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed (simpleTabbed)
import XMonad.Layout.Renamed
import XMonad.Config.Desktop
import XMonad.Util.Cursor
import Data.Monoid
import System.Exit
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.WindowSwallowing --dependent on pstree
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Hooks.DynamicLog
import System.IO
import XMonad.Actions.RotSlaves
import XMonad.Actions.CycleWindows
import Graphics.X11.ExtraTypes.XF86
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName --for isabelle/other java applications to run properly
import XMonad.Util.NamedScratchpad
import XMonad.Hooks.InsertPosition (insertPosition, Focus(Newer), Position(End))

import Colors.SolarizedDark
-- import Colors.SolarizedLight

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "alacritty"
myTextEditor    = "vim"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = True

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
xmobarEscape = concatMap doubleLts
  where doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape) $ ["1","2","3","4","5","6","7","8"]

  where
         clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                             (i,ws) <- zip [1..8] l,
                            let n = i ]
-- clickable workspaces need xdotool to work

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor   = "#3b4252"
myFocusedBorderColor  = "#bc96da"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm,               xK_Escape ), spawn $ XMonad.terminal conf)

    -- launch dmenu and dashboard
    , ((modm,               xK_o     ), spawn "dmenu_run")

    -- launch scratchpad
    , ((modm,               xK_s), namedScratchpadAction myScratchPads "Terminal")

    -- lock the screen
    , ((modm .|. shiftMask, xK_x     ), spawn "slock"    )

    -- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm .|. shiftMask, xK_Tab   ), windows W.focusUp  )

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Rotate windows clockwise
    , ((modm,               xK_r     ), rotAllDown  )

    -- Rotate windows clockwise
    , ((modm .|. shiftMask, xK_r     ), rotAllUp    )

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Change Keyboard Layout
    ,((modm               , xK_c), spawn "setxkbmap Jamie-Dvorak")
    ,((modm               , xK_u), spawn "setxkbmap my_gb")

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), spawn "rofi -show p -modi p:rofi-power-menu -padding 0 -theme Paper -font \"Ubuntu Mono Nerd Font 16\" -width 20 -lines 6")

    -- Audio keys
    , ((0,                    xF86XK_AudioRaiseVolume), spawn "changevolume up")
    , ((0,                    xF86XK_AudioLowerVolume), spawn "changevolume down")
    , ((0,                    xF86XK_AudioMute), spawn "changevolume mute")

    -- Brightness keys
    , ((0,                    xF86XK_MonBrightnessUp), spawn "changebrightness up")
    , ((0,                    xF86XK_MonBrightnessDown), spawn "changebrightness down")

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_8]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:
myLayout = renamed [Replace ""] $ smartSpacing 4 $
           avoidStruts (smartBorders (tiled ||| simpleTabbed))
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "Terminal" spawnTerminal findTerminal manageTerminal ]
    where
    spawnTerminal  = myTerminal ++ " -t Scratchpad"
    findTerminal   = title =? "Scratchpad"
    manageTerminal = customFloating $ W.RationalRect x y w h
               where
                 h = 0.65
                 w = 0.65
                 y = 0.825 -h
                 x = 0.825 -w

-- Window rules:
myManageHook = composeAll
    [ className =? "MPlayer"         --> doFloat
    , insertPosition End Newer -- open new windows at the end
    , className =? "confirm"         --> doFloat
    , className =? "file_progress"   --> doFloat
    , className =? "dialog"          --> doFloat
    , className =? "download"        --> doFloat
    , className =? "error"           --> doFloat
    , className =? "notification"    --> doFloat
    , className =? "Gimp"            --> doFloat
    , resource  =? "desktop_window"  --> doIgnore
    , resource  =? "kdesktop"        --> doIgnore
    ] <+> namedScratchpadManageHook myScratchPads



------------------------------------------------------------------------
-- Startup hook
myStartupHook = do
        setWMName "LG3D"
        setDefaultCursor xC_left_ptr
        spawnOnce "nitrogen --restore &"
        spawnOnce "picom --experimental-backend &"
        spawnOnce "redshift-gtk -l 52.4543:0.0000 -t 6500:3600"
        spawnOnce "earlyoom -m 5 -s 5"
        spawnOnce "setxkbmap my_gb"
        spawnOnce "nm-applet"
        spawnOnce "dunst"
        spawn "exec ~/.local/bin/battery-notification"
        spawn "killall trayer"
        spawn "killall conky"   -- kill current conky on each restart
        spawn "sleep 2 && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0 --height 22"
        spawn ("sleep 2 && conky -c $HOME/.config/conky/" ++ colorScheme ++ "-01.conkyrc")


------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar $HOME/.xmonad/xmobarrc"
    xmonad $ ewmhFullscreen . ewmh $ def
        { manageHook = manageDocks <+> myManageHook -- make sure to include myManageHook definition from above
        , workspaces = myWorkspaces
        , handleEventHook = swallowEventHook (className =? "Alacritty") (return True)
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor "yellow" "" . wrap "[" "]"
                        , ppHiddenNoWindows = xmobarColor "grey" ""
                        , ppTitle   = xmobarColor "green"  "" . shorten 60
                        , ppHidden = xmobarColor "green" ""
                        , ppUrgent  = xmobarColor "red" "yellow"
                        },
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        startupHook        = myStartupHook
    }


